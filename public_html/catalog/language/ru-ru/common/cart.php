<?php
// Text
//$_['text_items']     = '%s товар(ов) - %s';
$_['text_items']     = '<span class="left">В корзине %s<br> %s </span><strong class="right">%s</strong>';
//$_['text_empty']     = 'В корзине пусто!';
$_['text_empty']     = '<span class="left">В корзине <br>пока пусто</span><strong class="right">%s</strong>';
$_['text_cart']      = 'Открыть корзину';
$_['text_checkout']  = 'Оформить заказ';
$_['text_recurring'] = 'Профиль платежа';
