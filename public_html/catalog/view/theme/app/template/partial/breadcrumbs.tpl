<!-- Responsive breadcrumbs -->
<div class="bread-crumbs" id="breadcrumbWrapee">
    <ul id="breadcrumbContainer" >
    </ul>
</div>
<!--Particular breadcrumbs' templates -->
<script id="breadcrumbTemplate" type="text/template">
    <li>
        <a href="<%href%>">
            <%text%>
        </a>
    </li>
</script>
<script id="homeIconTemplate" type="text/template">
     <li class="home">
        <a href="/">
            <i class="material-icons">home</i>
        </a>
     </li>
</script>
<script id="intervalTemplate" type="text/template">
     <li class="interval">...</li>
</script>

