import './lib/jquery.tabs';

/* Табы вида (грида) в категории */
// page init
$(function(){
	initTabs();
});

// content tabs init
function initTabs() {
	jQuery('ul.fade-tabset').contentTabs({
		autoHeight: true,
		animSpeed: 300,
		effect: 'fade',
		tabLinks: 'a',
		event: 'mouseenter'
	});
	jQuery('ul.slide-tabset').contentTabs({
		addToParent: true,
		animSpeed: 500,
		effect: 'slide',
		tabLinks: 'a'
	});
	jQuery('ul.multi-tabset').contentTabs({
		tabLinks: 'a'
	});
}